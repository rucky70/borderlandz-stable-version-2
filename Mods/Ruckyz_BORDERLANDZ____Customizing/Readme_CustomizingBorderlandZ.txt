
Customizing BorderlandZ
=======================

1) Reducing Treecount in the Biomes Snow, Burnt and Pine Forrest :

   biomes.xml : Disable/Enable what Option you want to have.
                Available : 100% Prop (Original HDHQ Biome Modlet), 25%, 10% and 1% Prop.
                
2) Adding more Starting Equipment

   loot.xml : add additional items to the lootgroup
   
3) Removing HitBar showing for Zombies and Animals

   loot.xml : remove "modVisor" from the lootgroup
   
4) Adjusting Vehicles PickUp Command

   vehicles.xml : change or delete the vehicles you want to be pickedUp
   
5)